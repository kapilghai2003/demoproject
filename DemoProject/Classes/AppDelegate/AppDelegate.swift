//
//
//
//
//


import UIKit
import UserNotifications
import IQKeyboardManagerSwift
import ZKDrawerController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var loginModel: LoginModel!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        
        let userDefault = getDefaultUser()
        if userDefault.value(forKey: kPreferenceDataISLogin) != nil {
            loginSucess()
        }
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return false
    }

    // MARK: Private Methods Methods
    func callLogin(identifier: String, storyboardType: String ) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: storyboardType, bundle:nil)
        let dashboardVC = storyBoard.instantiateViewController(withIdentifier: identifier)
        
        let leftVC = storyBoard.instantiateViewController(withIdentifier: "LeftMenuSB")
        let drawer = ZKDrawerController.init(center: dashboardVC, right: nil, left: leftVC)
        //drawer.gestureRecognizerWidth = 200
        //drawer.defaultRightWidth = 200
        //drawer.mainScale = 0.8
        drawer.containerView.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        drawer.drawerStyle = .plain
        drawer.gestureRecognizerWidth = 0.0        
        drawer.mainCoverView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        window?.rootViewController = drawer
    }

    //MARK: Public Methods
    func showAlertDialog(title : String , message : String )
    {
        
        //let message = kNSLocalizedString(a: message, b: "")
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    func loginSucess() {
        
        let userDefaults = getDefaultUser()
        let userString =  userDefaults.value(forKeyPath: kPreferenceDataLogin) as! String
        loginModel = LoginModel(JSONString: userString)
        
        callLogin(identifier: "DashboardSB", storyboardType: "Main")
    }
    
    func logoutSucess() {

        let userDefaults = getDefaultUser()
        userDefaults.set(nil, forKey: kPreferenceDataISLogin)
        userDefaults.set(nil, forKey: kPreferenceDataISLogin)
        userDefaults.synchronize()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "LoginSB")
        window?.rootViewController = secondViewController
    }
}
