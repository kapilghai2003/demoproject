//
//
//
//
//

import Foundation
import UIKit
import Alamofire

class ForgetPassViewModel {
    
    private let forgetManager: DataManager
    
    private var userEmail = ""
    
    var verificationCode: Int?
    var token: String?
    var message = ""
    
    private var credentials = Credentials() {
        didSet {
            userEmail = credentials.userEmail
        }
    }
    
    var credentialsInputErrorMessage: Observable<String> = Observable("")
    var isUserEmailTextFieldHighLighted: Observable<Bool> = Observable(false)
    var isShowProgress: Observable<Bool> = Observable(false)

    var errorMessage: Observable<String> = Observable("")
    var isSuccess: Observable<Bool> = Observable(false)
    
    
    init(forgetManager: DataManager) {
        self.forgetManager = forgetManager
    }
    
    //Here we update our model
    func updateCredentials(userEmail: String) {
        credentials.userEmail = userEmail
    }
    
    
    func forgetPassword() {
        
        let array = kForgetPasswordAPIParams.split(separator: "&").map { (subString) in
            return String(subString)
        }
        
        isShowProgress.value = true
        
        let params = [array[0]: userEmail]
        let method: HTTPMethod = .post
        dataManager.sendPostRequestAlamofire(url: kForgetPasswordAPI, param: params as [String : Any], method: method, completion: { [self] (response: ForgetResponse) in
            
            self.isShowProgress.value = false
            guard let error = response.error else {
                self.errorMessage.value = "There are some error."
                return
            }
            
            if (!error) {
                
                self.verificationCode = response.code!
                //self.verificationCode = response.to
//                credentialsInputErrorMessage.value = response.message!
                self.token = response.token!
                self.message = response.message!
                self.isSuccess.value = true
            } else {
                self.errorMessage.value = response.message!
            }
            
        }) {
            self.isShowProgress.value = false
        }
    }
    
    
    func credentialsInput() -> CredentialsInputStatus {
       
        if userEmail.isEmpty {
            credentialsInputErrorMessage.value = "Email field is empty."
            isUserEmailTextFieldHighLighted.value = true
            return .Incorrect
        }
        if !isValidEmail(email: userEmail) {
            credentialsInputErrorMessage.value = "Please enter valid emaild address."
            isUserEmailTextFieldHighLighted.value = true
            return .Incorrect
        }
        
        return .Correct
    }
}
