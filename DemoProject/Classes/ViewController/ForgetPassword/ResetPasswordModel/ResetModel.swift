//
//
//
//
//

import Foundation
import UIKit
import Alamofire

class ResetModel {
    
    private let resetManager: DataManager
    
    private var codeOne = ""
    private var codeTwo = ""
    private var codeThree = ""
    private var codeFour = ""
    private var codeFive = ""
    private var codeSix = ""
    private var newPassword = ""
    
    private var token = ""
    private var serverVerificationCode = ""
    var message = ""
    
    
    private var credentials = ResetPassword() {
        didSet {
            codeOne = credentials.codeOne
            codeTwo = credentials.codeTwo
            codeThree = credentials.codeThree
            codeFour = credentials.codeFour
            codeFive = credentials.codeFive
            codeSix = credentials.codeSix
            newPassword = credentials.newPassword
        }
    }
    
    var credentialsInputErrorMessage: Observable<String> = Observable("")
    var isCodeOneTextFieldHighLighted: Observable<Bool> = Observable(false)
    var isCodeTwoTextFieldHighLighted: Observable<Bool> = Observable(false)
    var isCodeThreeFieldHighLighted: Observable<Bool> = Observable(false)
    var isCodeFourFieldHighLighted: Observable<Bool> = Observable(false)
    var isCodeFiveFieldHighLighted: Observable<Bool> = Observable(false)
    var isCodeSixFieldHighLighted: Observable<Bool> = Observable(false)
    var isNewPasswordFieldHighLighted: Observable<Bool> = Observable(false)

    var isShowProgress: Observable<Bool> = Observable(false)
    
    var errorMessage: Observable<String> = Observable("")
    var isSuccess: Observable<Bool> = Observable(false)
    
    init(resetManager: DataManager) {
        self.resetManager = resetManager
    }
    
    //Here we update our model
    func updateCredentials(token: String, serverVerificationCode: String) {
        self.token = token
        self.serverVerificationCode = serverVerificationCode
    }
    
    func updateCredentials(codeOne: String, codeTwo: String, codeThree: String,
                           codeFour: String, codeFive: String, codeSix: String, newPassword: String) {
        credentials.codeOne = codeOne
        credentials.codeTwo = codeTwo
        credentials.codeThree = codeThree
        credentials.codeFour = codeFour
        credentials.codeFive = codeFive
        credentials.codeSix = codeSix
        credentials.newPassword = newPassword
    }
    
    
    func resetPassword() {
        
        let array = kResetPasswordAPIParams.split(separator: "&").map { (subString) in
            return String(subString)
        }
        
        isShowProgress.value = true
        let params = [array[0]: token, array[1]: "\(codeOne)\(codeTwo)\(codeThree)\(codeFour)\(codeFive)\(codeSix)", array[2]: newPassword]
        
        let method: HTTPMethod = .post
        dataManager.sendPostRequestAlamofire(url: kResetPasswordAPI, param: params as [String : Any], method: method, completion: { [self] (response: LoginModel) in
            
            guard let error = response.error else {
                self.errorMessage.value = "There are some error."
                return
            }
            
            if (!error) {
                self.message = response.message!
                self.isSuccess.value = true
            } else {
                self.isShowProgress.value = false
                self.errorMessage.value = response.message!
            }
            
        }) {
            self.isShowProgress.value = false
        }
    }
    
    func credentialsInput() -> CredentialsInputStatus {
            
        if codeOne.isEmpty && codeTwo.isEmpty  && codeThree.isEmpty
            && codeFour.isEmpty  && codeFive.isEmpty && codeSix.isEmpty && newPassword.isEmpty{
            credentialsInputErrorMessage.value = "Please provide all details."
            return .Incorrect
        }
        if codeOne.isEmpty {
            credentialsInputErrorMessage.value = "Code field is empty."
            isCodeOneTextFieldHighLighted.value = true
            return .Incorrect
        }
        if codeTwo.isEmpty {
            credentialsInputErrorMessage.value = "Code field is empty."
            isCodeTwoTextFieldHighLighted.value = true
            return .Incorrect
        }
        if codeThree.isEmpty {
            credentialsInputErrorMessage.value = "Code field is empty."
            isCodeThreeFieldHighLighted.value = true
            return .Incorrect
        }
        if codeFour.isEmpty {
            credentialsInputErrorMessage.value = "Code field is empty."
            isCodeFourFieldHighLighted.value = true
            return .Incorrect
        }
        if codeFive.isEmpty {
            credentialsInputErrorMessage.value = "Code field is empty."
            isCodeFiveFieldHighLighted.value = true
            return .Incorrect
        }
        if codeSix.isEmpty {
            credentialsInputErrorMessage.value = "Code field is empty."
            isCodeSixFieldHighLighted.value = true
            return .Incorrect
        }
        
        if newPassword.isEmpty {
            credentialsInputErrorMessage.value = "New Password field is empty."
            isNewPasswordFieldHighLighted.value = true
            return .Incorrect
        }
        return .Correct
    }
}
