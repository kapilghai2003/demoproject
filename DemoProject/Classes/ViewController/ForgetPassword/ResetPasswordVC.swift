//
//
//
//
//

import UIKit
import MBProgressHUD

class ResetPasswordVC:  UIViewController {
    
    var resetViewModel: ResetModel!
    
    var token: String!
    var verificationCode: String!
    
    
    @IBOutlet weak var codeOneTextField: UITextField!
    @IBOutlet weak var codeTwoTextField: UITextField!
    @IBOutlet weak var codeThreeTextField: UITextField!
    @IBOutlet weak var codeFourTextField: UITextField!
    @IBOutlet weak var codeFiveTextField: UITextField!
    @IBOutlet weak var codeSixTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    
    @IBOutlet weak var resetButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetting()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    //MARK: Private methods
    func viewSetting() {

        resetButton.layer.cornerRadius = 4.5
        resetViewModel = ResetModel(resetManager: dataManager)
        resetViewModel.updateCredentials(token: token, serverVerificationCode: verificationCode)
        bindData()
        setupTextField()
        codeOneTextField.becomeFirstResponder()
    }
    
    func showAlertDialog(title : String , message : String )
    {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        if isIphone() {
            if let presenterController = alert.popoverPresentationController {
                presenterController.sourceView = self.view;
            }
        }
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func bindData() {
        
        resetViewModel.isShowProgress.bind {
            
            if $0 {
                MBProgressHUD.showAdded(to: self.view, animated: true)
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
        
        resetViewModel.credentialsInputErrorMessage.bind {
            appDelgate.showAlertDialog(title: "", message: $0)
        }
        

        resetViewModel.isCodeOneTextFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.codeOneTextField.becomeFirstResponder()
            }
        }
        
        resetViewModel.isCodeTwoTextFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.codeTwoTextField.becomeFirstResponder()
            }
        }
        
        resetViewModel.isCodeThreeFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.codeThreeTextField.becomeFirstResponder()
            }
        }
        
        resetViewModel.isCodeFourFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.codeFourTextField.becomeFirstResponder()
            }
        }
        
        resetViewModel.isCodeFiveFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.codeFiveTextField.becomeFirstResponder()
            }
        }
        
        resetViewModel.isCodeSixFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.codeSixTextField.becomeFirstResponder()
            }
        }
        
        resetViewModel.isNewPasswordFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.newPasswordTextField.becomeFirstResponder()
            }
        }
        
        resetViewModel.errorMessage.bind {
            appDelgate.showAlertDialog(title: "", message: $0)
        }
        
        resetViewModel.isSuccess.bind { [self] in
            if $0 {
                self.showAlertDialog(title: "", message: resetViewModel.message)
            }
        }
    }
    
    func setupTextField() {
        let textFields = [codeOneTextField, codeTwoTextField, codeThreeTextField, codeFourTextField, codeFiveTextField, codeSixTextField]
        textFields.forEach { (textfield) in
            textfield!.tintColor = .white
            textfield!.keyboardType = .numberPad
            textfield!.delegate = self
            textfield!.addBottomBorder(value: -2, color: UIColor.darkGray)
        }
    }
    
    //MARK: Button methods
    @IBAction func backAction(_sender : UIButton) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func resetAction(_sender : UIButton) {
        
        self.view.endEditing(true)
        
        resetViewModel.updateCredentials(codeOne: codeOneTextField.text!, codeTwo: codeTwoTextField.text!, codeThree: codeThreeTextField.text!, codeFour: codeFourTextField.text!, codeFive: codeFiveTextField.text!, codeSix: codeSixTextField.text!, newPassword: newPasswordTextField.text!)
        
        //Here we check user's credentials input - if it's correct we call login()
        switch resetViewModel.credentialsInput() {
            
        case .Correct:
            resetViewModel.resetPassword()
        case .Incorrect:
            return
        }
    }
}

extension ResetPasswordVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ((textField.text?.count)! < 1  && string.count > 0) || (textField.text?.count == 1 && string.count > 0) {
            let nextTag = textField.tag + 1
            
            // get next responder
            let nextResponder = textField.superview?.viewWithTag(nextTag)
            textField.text = string
            
            if (nextResponder == nil) {
                textField.resignFirstResponder()
            }
            nextResponder?.becomeFirstResponder()
            return false
        }
        else if ((textField.text?.count)! >= 1  && string.count == 0) {
            // on deleting value from Textfield
            let previousTag = textField.tag - 1;
            
            // get previous responder
            var previousResponder = textField.superview?.viewWithTag(previousTag)
            
            if (previousResponder == nil) {
                previousResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = ""
            previousResponder?.becomeFirstResponder()
            return false
        }
        return true
    }
}

extension UITextField {
    func addBottomBorder(value:CGFloat = 1,color:UIColor = .black){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height + value, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = color.cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
}
