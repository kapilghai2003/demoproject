//
//
//
//
//

import UIKit
import MBProgressHUD

class ForgetPasswordVC:  UIViewController {
    
    var forgetViewModel: ForgetPassViewModel!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetting()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination.isKind(of: ResetPasswordVC.self) {
            let resetVC = segue.destination as! ResetPasswordVC
            resetVC.token = forgetViewModel.token!
            resetVC.verificationCode = "\(forgetViewModel.verificationCode!)"
        }
    }
    
    //MARK: Private methods
    func viewSetting() {

        emailButton.layer.cornerRadius = 4.5
        forgetViewModel = ForgetPassViewModel(forgetManager: dataManager)
        bindData()
    }
    
    func showAlertDialog(title : String , message : String )
    {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert) in
            self.performSegue(withIdentifier: "resetPasswordSegue", sender: nil)
        }))
        
        if isIphone() {
            if let presenterController = alert.popoverPresentationController {
                presenterController.sourceView = self.view;
            }
        }
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func bindData() {
        
        forgetViewModel.isShowProgress.bind {
            
            if $0 {
                MBProgressHUD.showAdded(to: self.view, animated: true)
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
        
        forgetViewModel.credentialsInputErrorMessage.bind {
            appDelgate.showAlertDialog(title: "", message: $0)
        }
        
        forgetViewModel.isUserEmailTextFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.emailTextField.becomeFirstResponder()
            }
        }
        
        forgetViewModel.errorMessage.bind {
            appDelgate.showAlertDialog(title: "", message: $0)
        }
        
        forgetViewModel.isSuccess.bind { [self] in
            if $0 {
                self.showAlertDialog(title: "", message: forgetViewModel.message)
            }
        }
    }
    
    //MARK: Button methods
    @IBAction func backAction(_sender : UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgetAction(_sender : UIButton) {
        
        self.view.endEditing(true)
        
        forgetViewModel.updateCredentials(userEmail: emailTextField.text!)
        
        //Here we check user's credentials input - if it's correct we call login()
        switch forgetViewModel.credentialsInput() {
            
        case .Correct:
            forgetViewModel.forgetPassword()
        case .Incorrect:
            return
        }
    }
}
