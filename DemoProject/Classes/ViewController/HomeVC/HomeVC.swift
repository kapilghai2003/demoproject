//
//
//
//
//

import UIKit

class HomeVC:  UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetting()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    //MARK: Private methods
    func viewSetting() {
        
    }
    
    func showLogoutAlertDialog(title : String , message : String ) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default, handler: {(alert) in
            appDelgate.logoutSucess()
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: {(alert) in
            
        }))
        
        if isIphone() {
            if let presenterController = alert.popoverPresentationController {
                presenterController.sourceView = self.view;
            }
        }
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK: Button methods
    @IBAction func leftNavAction(_sender : UIButton){
        
        drawerController!.show(.left, animated: true)
    }
    
    // MARK: Public Methods
    func leftMenuSelected(index: Int) {
        
    }
    
    func logoutUser() {
        
        self.showLogoutAlertDialog(title: "WARNING!", message: "Are you sure, you want to logout?")
    }
}
