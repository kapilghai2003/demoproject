//
//
//
//
//

import UIKit

class LeftMenuVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var logoutButton: UIButton!
    
    var listText = ["View 1", "View 2", "View 3", "View 4", "View 5"]
    
    // MARK: UIWork FLow
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewSetting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
    }
    
    // MARK: Private Methods
    func viewSetting() {
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        logoutButton.layer.cornerRadius = 4.5
    }
    
    //MARK: Button methods
    @IBAction func logoutAction(_sender : UIButton){
        
        if let drawerVC = drawerController, let navVC = drawerVC.centerViewController as? UINavigationController, let homeVC = navVC.visibleViewController as? HomeVC {
            
            drawerVC.hide(animated: true)
            homeVC.logoutUser()
        }
    }
}

extension LeftMenuVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)
        
        if let drawerVC = drawerController, let navVC = drawerVC.centerViewController as? UINavigationController, let homeVC = navVC.visibleViewController as? HomeVC {
            
            drawerVC.hide(animated: true)
            homeVC.leftMenuSelected(index: indexPath.row)
        }
    }
}

extension LeftMenuVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.listText.count;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let imageView = cell.viewWithTag(101) as! UIImageView
        let textLabel = cell.viewWithTag(102) as! UILabel
        
        textLabel.text = self.listText[indexPath.row]
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        
        return cell
    }
    
    
}

