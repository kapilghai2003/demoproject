//
//
//
//
//

import Foundation
import UIKit
import Alamofire

class SignupModel {
    
    private let signupManager: DataManager
    
    private var userFirstName = ""
    private var userLastName = ""
    private var userCode = ""
    private var userEmail = ""
    private var userPassword = ""
    private var userRole = ""
    
    private var credentials = Credentials() {
        didSet {
            userEmail = credentials.userEmail
            userPassword = credentials.password
            userRole = credentials.userRole
            userFirstName = credentials.userFirstName
            userLastName = credentials.userLastName
            userCode = credentials.userCode
        }
    }
    
    var credentialsInputErrorMessage: Observable<String> = Observable("")
    var isFirstNameTextFieldHighLighted: Observable<Bool> = Observable(false)
    var isLastNameTextFieldHighLighted: Observable<Bool> = Observable(false)
    var isCodeTextFieldHighLighted: Observable<Bool> = Observable(false)
    var isEmailTextFieldHighLighted: Observable<Bool> = Observable(false)
    var isPasswordTextFieldHighLighted: Observable<Bool> = Observable(false)
    var isRoleTextFieldHighLighted: Observable<Bool> = Observable(false)

    var isShowProgress: Observable<Bool> = Observable(false)
    
    var errorMessage: Observable<String> = Observable("")
    var isSuccess: Observable<Bool> = Observable(false)
    
    init(signupManager: DataManager) {
        self.signupManager = signupManager
    }
    
    //Here we update our model
    func updateCredentials(userEmail: String, password: String, userRole: String,
                           userCode: String, userFirstName: String, userLastName: String) {
        credentials.userEmail = userEmail
        credentials.password = password
        credentials.userRole = userRole
        credentials.userCode = userCode
        credentials.userFirstName = userFirstName
        credentials.userLastName = userLastName
    }
    
    
    func signUp() {
        
        let array = kSingupAPIParams.split(separator: "&").map { (subString) in
            return String(subString)
        }
        
        isShowProgress.value = true
        let params = [array[0]: userEmail, array[1]: userPassword, array[2]: userRole]
        let method: HTTPMethod = .post
        dataManager.sendPostRequestAlamofire(url: kSingupAPI, param: params as [String : Any], method: method, completion: { [self] (response: LoginModel) in
            
            guard let error = response.error else {
                self.errorMessage.value = "There are some error."
                return
            }
            
            if (!error) {
                let userDefaults = getDefaultUser()
                userDefaults.set(response.toJSONString(), forKey: kPreferenceDataLogin)
                userDefaults.set("1", forKey: kPreferenceDataISLogin)
                userDefaults.synchronize()
                // Here we call Update API
                self.updateProfile(token: response.token!)
            } else {
                self.isShowProgress.value = false
                self.errorMessage.value = response.message!
            }
            
        }) {
            self.isShowProgress.value = false
        }
    }
    
    func updateProfile(token: String) {
        
        /*let array = kUpdateProfileAPIParams.split(separator: "&").map { (subString) in
            return String(subString)
        }
        
        let params = ["payload": [array[0]: userFirstName, array[1]: userLastName, array[2]: userCode]]*/
        
        
        var params = [String: Any]()
        var subDict = [String: Any]()
        subDict.updateValue(userFirstName, forKey: "first_name")
        subDict.updateValue(userLastName, forKey: "last_name")
        subDict.updateValue(userCode, forKey: "code")
        params.updateValue(subDict, forKey: "payload")
        
        
        let method: HTTPMethod = .post
        
        dataManager.sendPostRequestAlamofireJSON(url: kUpdateProfileAPI, param: params as [String : Any], authToken: token, method: method, completion: { [self] (response: LoginModel) in
            
            self.isShowProgress.value = false
            guard let error = response.error else {
                self.errorMessage.value = "There are some error."
                return
            }
            
            if (!error) {
                self.isSuccess.value = true
            } else {
                self.errorMessage.value = response.message!
            }
            
        }) {
            self.isShowProgress.value = false
        }
    }
    
    func credentialsInput() -> CredentialsInputStatus {
            
        if userFirstName.isEmpty && userLastName.isEmpty  && userCode.isEmpty
            && userEmail.isEmpty  && userPassword.isEmpty  && userRole.isEmpty{
            credentialsInputErrorMessage.value = "Please provide all details."
            return .Incorrect
        }
        if userFirstName.isEmpty {
            credentialsInputErrorMessage.value = "First Name field is empty."
            isFirstNameTextFieldHighLighted.value = true
            return .Incorrect
        }
        if userLastName.isEmpty {
            credentialsInputErrorMessage.value = "Last Name field is empty."
            isLastNameTextFieldHighLighted.value = true
            return .Incorrect
        }
        if userCode.isEmpty {
            credentialsInputErrorMessage.value = "Code field is empty."
            isCodeTextFieldHighLighted.value = true
            return .Incorrect
        }
        if userEmail.isEmpty {
            credentialsInputErrorMessage.value = "Email field is empty."
            isEmailTextFieldHighLighted.value = true
            return .Incorrect
        }
        if !isValidEmail(email: userEmail) {
            credentialsInputErrorMessage.value = "Please enter valid emaild address."
            isEmailTextFieldHighLighted.value = true
            return .Incorrect
        }
        if userPassword.isEmpty {
            credentialsInputErrorMessage.value = "Password field is empty."
            isPasswordTextFieldHighLighted.value = true
            return .Incorrect
        }
        if userRole.isEmpty {
            credentialsInputErrorMessage.value = "Role field is empty."
            isRoleTextFieldHighLighted.value = true
            return .Incorrect
        }
        return .Correct
    }
}
