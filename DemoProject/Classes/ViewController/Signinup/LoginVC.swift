//
//
//
//
//

import UIKit
import MBProgressHUD

class LoginVC:  UIViewController {
    
    var loginViewModel: LoginViewModel!
    
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var roleTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var newUserButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetting()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    //MARK: Private methods
    func viewSetting() {
        //emailTextField.text = "sk.kapilk@gmail.com"
        //passwordTextField.text = "123456"
        loginButton.layer.cornerRadius = 4.5
        newUserButton.layer.cornerRadius = 4.5
        
        loginViewModel = LoginViewModel(loginManager: dataManager)
        
        bindData()
    }
    
    func bindData() {
        
        loginViewModel.isShowProgress.bind {
            
            if $0 {
                MBProgressHUD.showAdded(to: self.view, animated: true)
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
        
        loginViewModel.credentialsInputErrorMessage.bind {
            appDelgate.showAlertDialog(title: "", message: $0)
        }
        
        loginViewModel.isUsernameTextFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.emailTextField.becomeFirstResponder()
            }
        }
        
        loginViewModel.isPasswordTextFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.passwordTextField.becomeFirstResponder()
            }
        }
        
        loginViewModel.isRoleTextFieldHighLighted.bind { [weak self] in
            if $0 {
                self?.roleTextField.becomeFirstResponder()
            }
        }
        
        loginViewModel.errorMessage.bind {
            appDelgate.showAlertDialog(title: "", message: $0)
        }
        
        loginViewModel.isSuccess.bind {
            if $0 {
                appDelgate.loginSucess()
            }
        }
    }
    
    //MARK: Button methods
    @IBAction func loginAction(_sender : UIButton){
        
        self.view.endEditing(true)
        
        loginViewModel.updateCredentials(userEmail: emailTextField.text!, password: passwordTextField.text!, userRole: roleTextField.text!)
        
        //Here we check user's credentials input - if it's correct we call login()
        switch loginViewModel.credentialsInput() {
            
        case .Correct:
            loginViewModel.login()
        case .Incorrect:
            return
        }
    }
    
    @IBAction func forgetAction(_sender : UIButton){
        
        self.view.endEditing(true)
    }
}
