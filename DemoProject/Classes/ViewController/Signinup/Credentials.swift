//
//
//
//
//

import Foundation

struct Credentials {
    var userEmail: String = ""
    var password: String = ""
    var userRole: String = ""
    
    var userFirstName: String = ""
    var userLastName: String = ""
    var userCode: String = ""
}
