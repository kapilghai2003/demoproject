//
//
//
//
//

import UIKit
import MBProgressHUD

class SignupVC:  UIViewController {
    
    var signUpViewModel: SignupModel!
    
    @IBOutlet weak var fNameTextField: UITextField!
    @IBOutlet weak var lNameTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var roleTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetting()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    //MARK: Private methods
    func viewSetting() {

        signUpButton.layer.cornerRadius = 4.5
        signUpViewModel = SignupModel(signupManager: dataManager)
        bindData()
    }
    
    func bindData() {
        
        signUpViewModel.isShowProgress.bind {
            
            if $0 {
                MBProgressHUD.showAdded(to: self.view, animated: true)
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
        
        signUpViewModel.credentialsInputErrorMessage.bind {
            appDelgate.showAlertDialog(title: "", message: $0)
        }
        
        signUpViewModel.isFirstNameTextFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.fNameTextField.becomeFirstResponder()
            }
        }
        
        signUpViewModel.isLastNameTextFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.lNameTextField.becomeFirstResponder()
            }
        }
        
        signUpViewModel.isCodeTextFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.codeTextField.becomeFirstResponder()
            }
        }
        
        signUpViewModel.isEmailTextFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.emailTextField.becomeFirstResponder()
            }
        }
        
        signUpViewModel.isPasswordTextFieldHighLighted.bind { [weak self] in
            
            if $0 {
                self?.passwordTextField.becomeFirstResponder()
            }
        }
        
        signUpViewModel.isRoleTextFieldHighLighted.bind { [weak self] in
            if $0 {
                self?.roleTextField.becomeFirstResponder()
            }
        }
        
        signUpViewModel.errorMessage.bind {
            appDelgate.showAlertDialog(title: "", message: $0)
        }
        
        signUpViewModel.isSuccess.bind {
            if $0 {
                appDelgate.loginSucess()
            }
        }
    }
    
    //MARK: Button methods
    @IBAction func backAction(_sender : UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpAction(_sender : UIButton) {
        
        self.view.endEditing(true)
        
        signUpViewModel.updateCredentials(userEmail: emailTextField.text!, password: passwordTextField.text!,
                                          userRole: roleTextField.text!, userCode: codeTextField.text!,
                                          userFirstName: fNameTextField.text!, userLastName: lNameTextField.text!)
        
        //Here we check user's credentials input - if it's correct we call login()
        switch signUpViewModel.credentialsInput() {
            
        case .Correct:
            signUpViewModel.signUp()
        case .Incorrect:
            return
        }
    }
}

extension SignupVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        if textField == fNameTextField || textField == lNameTextField {
            
            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
        }
        
        return true;
    }
}
