//
//
//
//
//

import Foundation
import UIKit
import Alamofire

class LoginViewModel {
    
    private let loginManager: DataManager
    
    private var userEmail = ""
    private var password = ""
    private var userRole = ""
    
    private var credentials = Credentials() {
        didSet {
            userEmail = credentials.userEmail
            password = credentials.password
            userRole = credentials.userRole
        }
    }
    
    var credentialsInputErrorMessage: Observable<String> = Observable("")
    var isUsernameTextFieldHighLighted: Observable<Bool> = Observable(false)
    var isPasswordTextFieldHighLighted: Observable<Bool> = Observable(false)
    var isRoleTextFieldHighLighted: Observable<Bool> = Observable(false)
    var isShowProgress: Observable<Bool> = Observable(false)
    
    var errorMessage: Observable<String> = Observable("")
    var isSuccess: Observable<Bool> = Observable(false)
    
    
    init(loginManager: DataManager) {
        self.loginManager = loginManager
    }
    
    //Here we update our model
    func updateCredentials(userEmail: String, password: String, userRole: String) {
        credentials.userEmail = userEmail
        credentials.password = password
        credentials.userRole = userRole
    }
    
    
    func login() {
        
        let array = kLoginAPIParams.split(separator: "&").map { (subString) in
            return String(subString)
        }
        
        
        isShowProgress.value = true
        
        let params = [array[0]: userEmail, array[1]: password, array[2]: userRole]
        let method: HTTPMethod = .post
        dataManager.sendPostRequestAlamofire(url: kLoginAPI, param: params as [String : Any], method: method, completion: { [self] (response: LoginModel) in
            
            self.isShowProgress.value = false
            guard let error = response.error else {
                self.errorMessage.value = "There are some error."
                return
            }
            
            if (!error) {
                let userDefaults = getDefaultUser()
                userDefaults.set(response.toJSONString(), forKey: kPreferenceDataLogin)
                userDefaults.set("1", forKey: kPreferenceDataISLogin)
                userDefaults.synchronize()
                
                self.isSuccess.value = true
            } else {
                self.errorMessage.value = response.message!
            }
            
        }) {
            self.isShowProgress.value = false
        }
    }
    
    
    func credentialsInput() -> CredentialsInputStatus {
        if userEmail.isEmpty && password.isEmpty {
            credentialsInputErrorMessage.value = "Please provide email and password."
            return .Incorrect
        }
        if userEmail.isEmpty {
            credentialsInputErrorMessage.value = "Email field is empty."
            isUsernameTextFieldHighLighted.value = true
            return .Incorrect
        }
        if !isValidEmail(email: userEmail) {
            credentialsInputErrorMessage.value = "Please enter valid emaild address."
            isUsernameTextFieldHighLighted.value = true
            return .Incorrect
        }
        if password.isEmpty {
            credentialsInputErrorMessage.value = "Password field is empty."
            isPasswordTextFieldHighLighted.value = true
            return .Incorrect
        }
        if userRole.isEmpty {
            credentialsInputErrorMessage.value = "Role field is empty."
            isRoleTextFieldHighLighted.value = true
            return .Incorrect
        }
        
        return .Correct
    }
}
