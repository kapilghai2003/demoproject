//
//
//
//
//


import Foundation
import UIKit
import Alamofire
import AlamofireImage

import AlamofireObjectMapper
import ObjectMapper

class DataManager: NSObject {
    
    static let dataManager = DataManager();
    
    override init(){
        
        super.init();
    }
    
    func sendPostRequestAlamofire<T: Mappable>(url: String, param: [String: Any], authToken: String? = nil, method: Alamofire.HTTPMethod,  completion: @escaping (T) -> Void, failure: @escaping () -> Void)  {
        
        #if DEBUG
            print(url)
            print(param)
        #endif
        
        if !isConnectedToNetwork() {
            
            DispatchQueue.main.async {
                appDelgate.showAlertDialog(title:"" , message: "Please check your internet connection.")
                failure()
            }
        } else {
            let headers = authToken != nil ? ["Content-Type": "application/json", "x-project": kProjectKey, "Authorization" : "Bearer \(authToken!)"]
            : ["Content-Type": "application/x-www-form-urlencoded", "x-project": kProjectKey]
                        
            Alamofire.request(url, method: method, parameters: param, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<T>) in
                
                DispatchQueue.main.async {
                    
                    if response.result.isSuccess {
                        
                        #if DEBUG
                            print(response.result.value?.toJSONString()! as Any)
                        #endif
                        
                        completion(response.result.value!)
                    } else {
                        appDelgate.showAlertDialog(title:"" , message: "There are some error. Please try after sometime.")
                        failure()
                    }
                }
            }
        }
    }
    
    func sendPostRequestAlamofireJSON<T: Mappable>(url: String, param: [String: Any], authToken: String? = nil,  method: Alamofire.HTTPMethod,  completion: @escaping (T) -> Void, failure: @escaping () -> Void)  {
        
        #if DEBUG
            print(url)
            print(param)
        #endif
        
        if !isConnectedToNetwork() {
            
            DispatchQueue.main.async {
                appDelgate.showAlertDialog(title:"" , message: "Please check your internet connection.")
                failure()
            }
        } else {
            let headers = authToken != nil ? ["Content-Type": "application/json", "x-project": kProjectKey, "Authorization" : "Bearer \(authToken!)"]
            : ["Content-Type": "application/json", "x-project": kProjectKey]
            
            
            Alamofire.request(url, method: method, parameters: param, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<T>) in
                
                DispatchQueue.main.async {
                    
                    if response.result.isSuccess {
                        
#if DEBUG
                        print(response.result.value?.toJSONString()! as Any)
#endif
                        completion(response.result.value!)
                    } else {
                        appDelgate.showAlertDialog(title:"" , message: "There are some error. Please try after sometime.")
                        failure()
                    }
                }
            }
        }
    }
    
}
