//
//
//
//
//

import Foundation
import ObjectMapper

class ForgetResponse: Mappable {
    
    var code: Int?
    var token: String?
    var message: String?
    var error: Bool?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        message <- map["message"]
        error <- map["error"]
        code <- map["code"]
        token <- map["token"]
    }
}
