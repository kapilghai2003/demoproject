//
//
//
//
//

import Foundation
import ObjectMapper

class LoginModel: Mappable {
    
    var token: String?
    var user_id: Int?
    var message: String?
    var error: Bool?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        message <- map["message"]
        error <- map["error"]
        token <- map["token"]
        user_id <- map["user_id"]
    }
}
