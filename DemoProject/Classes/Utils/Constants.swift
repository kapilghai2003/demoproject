//
//
//  
//
//

import UIKit
import SystemConfiguration
import Photos

let kPreferenceDataISLogin = "keyPreferenceData_ISLogin"
let kPreferenceDataLogin = "keyPreferenceData_LoginData"
let kProjectKey = "cmVhY3R0YXNrOjVmY2h4bjVtOGhibzZqY3hpcTN4ZGRvZm9kb2Fjc2t5ZQ=="

let appDelgate = UIApplication.shared.delegate as! AppDelegate
let dataManager = DataManager.dataManager



let kHostURL = "https://reacttask.mkdlabs.com/v2/api"

let kLoginAPI = String(format: "%@/lambda/login", kHostURL)
let kLoginAPIParams = "email&password&role";

let kSingupAPI = String(format: "%@/lambda/register", kHostURL)
let kSingupAPIParams = "email&password&role";

let kUpdateProfileAPI = String(format: "%@/lambda/profile", kHostURL)
let kUpdateProfileAPIParams = "first_name&last_name&code";

let kForgetPasswordAPI = String(format: "%@/lambda/forgot", kHostURL)
let kForgetPasswordAPIParams = "email";

let kResetPasswordAPI = String(format: "%@/lambda/reset", kHostURL)
let kResetPasswordAPIParams = "token&code&password";



enum CredentialsInputStatus {
    case Correct
    case Incorrect
}


public func isConnectedToNetwork() -> Bool {
    
    var zeroAddress = sockaddr_in()
    
    zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
    
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1){
            
            SCNetworkReachabilityCreateWithAddress(nil, $0)
            
        }
        
    })
    else
    
    {
        return false
        
    }
    
    var flags: SCNetworkReachabilityFlags = []
    
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        
        return false
        
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    
    return (isReachable && !needsConnection)    
}

public func getDefaultUser() -> UserDefaults {
    
    return UserDefaults.standard
}

public func isValidEmail(email: String) -> Bool {
    
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: email);
}

public func isIphone() -> Bool {
    
    let model = UIDevice.current.model
    
    if model.range(of: "iPad", options: String.CompareOptions.caseInsensitive, range: model.range(of: "iPad"), locale: nil) != nil {
        
        return true
    } else {
        
        return false
    }
}
